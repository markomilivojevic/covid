<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Redis;
use App\Services\Point\PointService;

/**
 * Class PointsTableSeeder
 */
class PointsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @throws Exception
     * @return void
     */
    public function run()
    {
        Redis::flushDB();

        if (\Illuminate\Support\Facades\App::environment('local')) {
            $batchesCount = 50;
            $batchSize = 2000;
        } else {
            $batchesCount = 100;
            $batchSize = 3000;
        }

        for ($i = 1; $i <= $batchesCount; $i++) {
            $this->command->getOutput()->writeln("<comment>Seeding batch:</comment> {$i}/{$batchesCount}...");
            $start = microtime(true);
            $this->insertPoints($batchSize);
            $seconds = round(microtime(true) - $start, 2);
            $this->command->getOutput()->writeln("<comment>Seeding batch {$i} took {$seconds} seconds</comment>");

            sleep(10);
        }
    }

    /**
     * @param int $limit
     * @throws Exception
     */
    protected function insertPoints(int $limit)
    {
        $points = factory(App\Point::class, $limit)->make();

        $uuid = \App\Services\UUID::generate();

        /** @var  App\Point $point */
        foreach ($points as $point) {
            $lng = $point->getLng();
            $lat = $point->getLat();
            $timestamp = $point->reported_at->getTimestamp();

            if (!$uuid) {
                throw new Exception('Failed to generate uuid for seed');
            }

            PointService::savePoint($uuid, $lng, $lat, $timestamp);

            if (rand(1, 10) > 3) {
                $uuid = \App\Services\UUID::generate();
            }
        }
    }
}
