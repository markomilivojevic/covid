<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('points', 'API\PointController@index');
Route::post('points', 'API\PointController@store');

Route::get('uuid/{uuid}/points', 'API\UserPointController@index');
Route::post('uuid/{uuid}/delete-point', 'API\UserPointController@removePoint');
