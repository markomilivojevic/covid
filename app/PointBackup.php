<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

/**
 * Class PointBackup
 * @package App
 */
class PointBackup extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['uuid', 'lng', 'lat', 'reported_at', 'deleted_at'];

    /**
     * @param Point $point
     * @return bool
     */
    public static function backup(Point $point)
    {
        try {
            return (bool)static::create([
                'uuid' => $point->uuid,
                'lng' => $point->lng,
                'lat' => $point->lat,
                'reported_at' => $point->reported_at,
                'deleted_at' => Carbon::createFromTimestamp(time())->toDateTimeString(),
            ]);
        } catch (\Exception $exception) {
            Log::error('Failed to backup point: ' . $exception->getMessage());
            return false;
        }
    }
}
