<?php

namespace App\Console\Commands;

use App\Services\Point\PointRedis;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;

/**
 * Class PopulatePointsRadiusSets
 * @package App\Console\Commands
 */
class PopulatePointsRadiusSets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'points:populate-radius-sets';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Populate sets for different radius';

    /**
     * Points per radius
     */
    const RADIUS_DENSITY = [
        // radius => points
        20000 => 1000,
        15000 => 2000,
        10000 => 4000,
        8000 => 8000,
        6000 => 12000,
        4000 => 16000,
        2000 => 20000,
    ];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach (static::RADIUS_DENSITY as $radius => $density) {
            $this->info(sprintf('Updating radius %d...', $radius));
            $points = PointRedis::getWithLimit($density);
            $this->info(sprintf('Points count %d', count($points)));
            PointRedis::savePointsByRadius($points, $radius);
        }
    }
}
