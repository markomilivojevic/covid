<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * Class Point
 *
 * @property string uuid
 * @property float lat
 * @property float lng
 * @property \DateTime reported_at
 * @package App
 */
class Point extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['uuid', 'lng', 'lat', 'reported_at'];

    /**
     * @return string
     */
    public function getUUID(): string
    {
        return $this->uuid;
    }

    /**
     * @return float
     */
    public function getLat(): float
    {
        return $this->lat;
    }

    /**
     * @return float
     */
    public function getLng(): float
    {
        return $this->lng;
    }

    /**
     * @return int
     */
    public function getReportedAtTimestamp(): int
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->reported_at)->getTimestamp();
    }

    /**
     * @return bool
     */
    public function deleteAndBackup(): bool
    {
        $deleted = DB::table('points')->where([
            'uuid' => $this->uuid,
            'lng' => $this->lng,
            'lat' => $this->lat,
            'reported_at' => $this->reported_at
        ])->delete();

        if ($deleted) {
            PointBackup::backup($this);
            return true;
        }

        return false;
    }

    /**
     * @param string $uuid
     * @param float $lng
     * @param float $lat
     * @param int $timestamp
     * @return bool
     */
    public static function savePoint(string $uuid, float $lng, float $lat, int $timestamp)
    {
        try {
            return static::create([
                'uuid' => $uuid,
                'lng' => $lng,
                'lat' => $lat,
                'reported_at' => Carbon::createFromTimestamp($timestamp)->toDateTimeString(),
            ]);
        } catch (\Exception $exception) {
            Log::error('Failed to create point: ' . $exception->getMessage());
            return false;
        }
    }
}
