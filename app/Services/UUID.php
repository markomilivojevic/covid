<?php

namespace App\Services;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Str;

/**
 * Class UUID
 * @package App\Services
 */
class UUID
{
    const UUIDS_KEY = 'uuids';

    /**
     * @param bool $retry
     * @return string
     */
    public static function generate(bool $retry = true): string
    {
        $uuid = Str::uuid()->toString();
        if (Redis::sAdd(static::UUIDS_KEY, $uuid)) {
            return $uuid;
        }

        Log::notice('Retrying to generate UUID');

        if (!$retry) {
            Log::error('Failed to generate UUID');
            return '';
        }

        return static::generate(false);
    }
}
