<?php

namespace App\Services\Point;

use App\Point as PointModel;

/**
 * Class PointService
 * @package App\Services\Point
 */
class PointService
{
    const DECIMAL_PRECISION = 6;

    /**
     * @param string $uuid
     * @param float $lng
     * @param float $lat
     * @param int $timestamp
     * @return bool
     */
    public static function savePoint(string $uuid, float $lng, float $lat, int $timestamp): bool
    {
        $lng = round($lng, static::DECIMAL_PRECISION);
        $lat = round($lat, static::DECIMAL_PRECISION);

        if (PointRedis::savePoint($uuid, $lng, $lat, $timestamp)) {
            if (PointModel::savePoint($uuid, $lng, $lat, $timestamp)) {
                return true;
            }

            PointRedis::removePoint($uuid, $timestamp);
        }

        return false;
    }

    /**
     * @param string $uuid
     * @param float $lng
     * @param float $lat
     * @return bool
     * @throws \Exception
     */
    public static function removePoint(string $uuid, float $lng, float $lat): bool
    {
        $lng = round($lng, static::DECIMAL_PRECISION);
        $lat = round($lat, static::DECIMAL_PRECISION);

        /** @var PointModel $point */
        $point = PointModel::where([
           'uuid' => $uuid,
           'lng' => $lng,
           'lat' => $lat,
        ])->first();

        if ($point) {
            if (PointRedis::removePoint($uuid, $point->getReportedAtTimestamp())) {
                if ($point->deleteAndBackup()) {
                    return true;
                }

                PointService::savePoint($uuid, $lng, $lat, $point->getReportedAtTimestamp());
            }
        }

        return false;
    }
}
