<?php

namespace App\Services\Point;

use Illuminate\Support\Facades\Redis;

/**
 * Class PointRedis
 * @package App\Services\Point
 */
class PointRedis
{
    const POINTS_KEY = 'points';
    const POINTS_RADIUS_KEY = 'points:';
    const MAX_RADIUS = 20000;

    const RADIUS_KEYS = [
        20000 => self::POINTS_RADIUS_KEY . 1000,
        15000 => self::POINTS_RADIUS_KEY . 2000,
        10000 => self::POINTS_RADIUS_KEY . 4000,
        8000 => self::POINTS_RADIUS_KEY . 8000,
        6000 => self::POINTS_RADIUS_KEY . 12000,
        4000 => self::POINTS_RADIUS_KEY . 16000,
        2000 => self::POINTS_RADIUS_KEY . 20000,
    ];

    /**
     * @param string $uuid
     * @param float $lng
     * @param float $lat
     * @param int $timestamp
     * @return bool
     */
    public static function savePoint(string $uuid, float $lng, float $lat, int $timestamp): bool
    {
        return (bool)Redis::geoAdd(static::POINTS_KEY, $lng, $lat, static::getPointKey($uuid, $timestamp));
    }

    /**
     * @param array $points
     * @param int $radius
     */
    public static function savePointsByRadius(array $points, int $radius)
    {
        Redis::del(static::RADIUS_KEYS[$radius]);
        foreach ($points as $point) {
            static::savePointByRadius($point[0], $point[1], $radius);
        }
    }

    /**
     * @param float $lng
     * @param float $lat
     * @param int $radius
     * @return bool
     */
    public static function savePointByRadius(float $lng, float $lat, int $radius)
    {
        return (bool)Redis::geoAdd(static::RADIUS_KEYS[$radius], $lng, $lat, "$lng:$lat");
    }

    /**
     * @param string $uuid
     * @param int $timestamp
     * @return bool
     */
    public static function removePoint(string $uuid, int $timestamp): bool
    {
        return (bool)Redis::zRem(static::POINTS_KEY, static::getPointKey($uuid, $timestamp));
    }

    /**
     * @param string $uuid
     * @param int $timestamp
     * @return string
     */
    protected static function getPointKey(string $uuid, int $timestamp): string
    {
        return "$uuid;$timestamp";
    }

    /**
     * @param float $lng
     * @param float $lat
     * @param float $radius
     * @return array
     */
    public static function getByRadius(float $lng, float $lat, float $radius): array
    {
        $key = static::POINTS_KEY;

        if ($radius > static::MAX_RADIUS) {
            $radius = static::MAX_RADIUS;
            $key = static::RADIUS_KEYS[static::MAX_RADIUS];
        } else {
            $minSetRadius = static::getMinSetRadius($radius);
            if ($minSetRadius !== false) {
                $key = static::RADIUS_KEYS[$minSetRadius];
            }
        }

        $points = Redis::geoRadius($key, $lng, $lat, $radius, 'km', ['WITHCOORD']);

        return static::preparePoints($points);
    }

    /**
     * @param int $searchRadius
     * @return bool|int|string
     */
    protected static function getMinSetRadius(int $searchRadius)
    {
        foreach (static::RADIUS_KEYS as $radius => $key) {
            if ($radius <= $searchRadius) {
                return $radius;
            }
        }

        return false;
    }

    /**
     * @return int
     */
    public static function count(): int
    {
        return Redis::zCard(PointRedis::POINTS_KEY);
    }

    /**
     * @param int $step
     * @param int $total
     * @return array
     */
    protected static function getMembersByStep(int $step, int $total): array
    {
        $members = [];
        for ($i = 0; $i < $total; $i += $step) {
            $member = Redis::zRange(static::POINTS_KEY, $i, $i);
            if (isset($member[0])) {
                $members[] = $member[0];
            }
        }

        return $members;
    }

    /**
     * @param int $limit
     * @return array
     */
    public static function getWithLimit(int $limit): array
    {
        $count = static::count();
        $step = intval(ceil($count / $limit));

        return static::getByMembers(static::getMembersByStep($step, $count));
    }

    /**
     * @param array $members
     * @return array
     */
    public static function getByMembers(array $members): array
    {
        return Redis::geoPos(static::POINTS_KEY, ...$members);
    }

    /**
     * @return array
     */
    public static function getAll(): array
    {
        return Redis::geoRadius(static::POINTS_KEY, 0, 0, static::MAX_RADIUS, 'km', ['WITHCOORD']);
    }

    /**
     * @param array $points
     * @return array
     */
    public static function preparePoints(array $points): array
    {
        $result = [];

        foreach ($points as $point) {
            $coords = $point[1];
            $result[] = [
                'longitude' => round($coords[0], PointService::DECIMAL_PRECISION),
                'latitude' => round($coords[1], PointService::DECIMAL_PRECISION),
            ];
        }

        return $result;
    }
}
