<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class PointResource
 * @package App\Http\Resources
 */
class PointResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'longitude' => $this->getLng(),
            'latitude' => $this->getLat(),
        ];
    }
}
