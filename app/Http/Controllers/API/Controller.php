<?php

namespace App\Http\Controllers\API;

/**
 * Class Controller
 * @package App\Http\Controllers\API
 */
abstract class Controller extends \App\Http\Controllers\Controller
{
    /**
     * @param array $data
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    public function response(array $data, int $code = 200)
    {
        return response()->json([
            'data' => $data['data'] ?? '',
            'message' => $data['message'] ?? '',
            'errors' => $data['errors'] ?? '',
        ], $code);
    }
}
