<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\GetPointsRequest;
use App\Http\Requests\StorePointRequest;
use App\Services\Point\PointRedis;
use App\Services\Point\PointService;
use App\Services\UUID;
use Illuminate\Support\Facades\Log;

/**
 * Class PointController
 * @package App\Http\Controllers\API
 */
class PointController extends Controller
{
    /**
     * @param GetPointsRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(GetPointsRequest $request)
    {
        $params = $request->validated();
        if ($params && isset($params['longitude']) && isset($params['latitude']) && isset($params['radius'])) {
            $start = microtime(true);
            $points = PointRedis::getByRadius($params['longitude'], $params['latitude'], $params['radius']);
            $time = microtime(true) - $start;

            Log::info(sprintf('REDIS DELTA: lng:%s, lat:%s, radius:%s. TIME: %s', $params['longitude'], $params['latitude'], $params['radius'], $time));

            return $this->response(['data' => $points]);
        }

        $start = microtime(true);
        $points = PointRedis::getByRadius(0, 0, PointRedis::MAX_RADIUS);
        $time = microtime(true) - $start;
        Log::info(sprintf('REDIS ALL: %s', $time));

        return $this->response(['data' => $points]);
    }

    /**
     * @param StorePointRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StorePointRequest $request)
    {
        $uuid = $request->validated()['uuid'] ?? '';
        $lng = $request->validated()['longitude'];
        $lat = $request->validated()['latitude'];

        if (!$uuid) {
            $uuid = UUID::generate();

            if (!$uuid) {
                return $this->response(['errors' => 'Failed to generate UUID.'], 500);
            }
        }

        $timestamp = time();
        if (PointService::savePoint($uuid, $lng, $lat, $timestamp)) {
            return $this->response(['data' => ['uuid' => $uuid], 'message' => 'Successfully stored!']);
        }

        Log::error(sprintf('Failed to save point %s', "$lng;$lat"));

        return $this->response(['errors' => 'Failed to save point.'], 500);
    }
}
