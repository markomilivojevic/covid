<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\RemovePointRequest;
use App\Http\Resources\PointResource;
use App\Point;
use App\Services\Point\PointService;
use Illuminate\Support\Facades\Validator;

/**
 * Class UserPointController
 * @package App\Http\Controllers\API
 */
class UserPointController extends Controller
{
    /**
     * @param string $uuid
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(string $uuid)
    {
        $validated = Validator::make(['uuid' => $uuid], ['uuid' => 'required|uuid'])->validate();
        $uuid = $validated['uuid'];

        $points = Point::where('uuid', $uuid)->get();

        return $this->response(['data' => PointResource::collection($points)]);
    }

    /**
     * @param RemovePointRequest $request
     * @param string $uuid
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function removePoint(RemovePointRequest $request, string $uuid)
    {
        $validated = Validator::make(['uuid' => $uuid], ['uuid' => 'required|uuid'])->validate();
        $uuid = $validated['uuid'];

        $lng = $request->validated()['longitude'];
        $lat = $request->validated()['latitude'];

        if (PointService::removePoint($uuid, $lng, $lat)) {
            return $this->response(['message' => 'Successfully removed.']);
        }

        return $this->response(['message' => 'Unable to remove point.'], 500);
    }
}
